<?php
$email = $_POST['email'];
$lengthNamaPeserta = strlen($email);
$fontSize = 40;
$emailArray = [];
$i=1;
if ($lengthNamaPeserta > 32) {
    $sisa = $lengthNamaPeserta - 32;
    
    if ($sisa > 6) {
        $emailArray = str_split($email, 32);
    }

    foreach($emailArray as $keyEmail => $valueEmail){
        if($i > 0){
            $fontSize -= 5;
        }
    }
}



$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
$fontDirs = $defaultConfig['fontDir'];

$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
$fontData = $defaultFontConfig['fontdata'];

$mpdf = new \Mpdf\Mpdf([
    'fontDir' => array_merge($fontDirs, [
        __DIR__ . '/vendor/mpdf/mpdf/ttfonts',
    ]),
    'fontdata' => $fontData + [
        'playball' => [
            'R' => 'Playball-Regular.ttf',
        ],
    ],
    'default_font' => 'playball'
]);


//ini code buat QR CODE
$isi_teks = "http://localhost/sertifikat-peserta/$email.pdf";
$namafile = "$email.png";
$qrCode = new QrCode();
// Set Text
$qrCode->setText($isi_teks);
$qrCode->setWriterByName('png');
$qrCode->setMargin(5);
$qrCode->setEncoding('UTF-8');
$qrCode->setErrorCorrectionLevel(new ErrorCorrectionLevel(ErrorCorrectionLevel::HIGH));
// Set Color
$qrCode->setRoundBlockSize(true);
$qrCode->setValidateResult(false);
$qrCode->setWriterOptions(['exclude_xml_declaration' => true]);
// Save it to a file
$qrCode->writeFile('./qr-code/'.$namafile);


//ini open pdf sertifikat
$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L', "fontdata" => $fontData, 'default_font' => 'playball']);
$pageCount = $mpdf->setSourceFile("sertifikat/sertifikat-pkg.pdf");
$tplId = $mpdf->importPage($pageCount);
$mpdf->SetPageTemplate($tplId);
$mpdf->WriteFixedPosHTML("
					<h1 style='font-family: playball; color: #2e56a6;font-size: {$fontSize}px;font-weight: 100;text-align:center;'>{$email}</h1>
                ", 65, 60, 162, 50);

$mpdf->WriteFixedPosHTML("
                   <img src='./qr-code/{$namafile}' width='85px'>
                ", 263, 178, 162, 50);

$mpdf->Output("./sertifikat-peserta/{$email}.pdf", 'F');
$mpdf->Output();
?>