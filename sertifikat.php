<?php
require_once __DIR__ . '/vendor/autoload.php';

include('library.php');
$lib = new Library();
if(isset($_POST['email'])){
    $email = $_POST['email']; 
    $data_peserta = $lib->cek_email($email); 
}else{
    $data_peserta = $lib->get_by_uniquecode($_GET["generate"]);
}

$url = "http://e-sertifikat.pkgonline.co.id/sertifikat.php?generate=";

if ($data_peserta == true) {
    $isi_teks = $url.$data_peserta[4];

    if($data_peserta[4] == "" || $data_peserta[4] == null){
        $id = $data_peserta[0];
        $kode = getName(5).date("Ymdhis");
        $isi_teks = $url.$kode;
        $status_update = $lib->addUrl($id, $isi_teks, $kode);
    }

    $email = $data_peserta[1];
    $namaPeserta = $data_peserta[2];

    $lengthNamaPeserta = strlen($namaPeserta);
    $fontSize = 40;
    $emailArray = [];

    $i=1;
    
    if ($lengthNamaPeserta > 32) {
        $sisa = $lengthNamaPeserta - 32;
        
        if ($sisa > 6) {
            $emailArray = str_split($namaPeserta, 32);
        }
    
        foreach($emailArray as $keyEmail => $valueEmail){
            if($i > 0){
                $fontSize -= 5;
            }
        }
    }
    
    
    $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
    $fontDirs = $defaultConfig['fontDir'];
    
    $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
    $fontData = $defaultFontConfig['fontdata'];
    
    $mpdf = new \Mpdf\Mpdf([
        'fontDir' => array_merge($fontDirs, [
            __DIR__ . '/vendor/mpdf/mpdf/ttfonts',
        ]),
        'fontdata' => $fontData + [
            'playball' => [
                'R' => 'Playball-Regular.ttf',
            ],
        ],
        'default_font' => 'playball'
    ]);
   
    //ini open pdf sertifikat
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L', "fontdata" => $fontData, 'default_font' => 'playball']);
    $pageCount = $mpdf->setSourceFile("sertifikat/sertifikat-pkg.pdf");
    for ($i=0;$i<$pageCount;$i++) {
        if($i==1){
            $mpdf->WriteFixedPosHTML("
                            <h1 style='font-family: playball; color: #2e56a6;font-size: {$fontSize}px;font-weight: 100;text-align:center;'>{$namaPeserta}</h1>
                        ", 65, 60, 162, 50);
        
            $mpdf->WriteFixedPosHTML("
                           <img src='./qrcode.php?generate={$isi_teks}' width='85px'>
                        ", 263, 178, 162, 50);
          
        }
        $tplId = $mpdf->importPage($i+1);
        $mpdf->SetPageTemplate($tplId);

        if($i==1){
            $mpdf->addPage();
        }
    }
    $mpdf->Output();

}else{
    // header('Location: tidak_terdaftar.php');
}

function getName($n) { 
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
    $randomString = ''; 
  
    for ($i = 0; $i < $n; $i++) { 
        $index = rand(0, strlen($characters) - 1); 
        $randomString .= $characters[$index]; 
    } 
  
    return $randomString; 
} 

?>