<?php
class Library
{
    public function __construct()
    {
        $host = "localhost";
        $dbname = "sertifikat-peserta-pkg";
        $username = "root";
        $password = "";
        $this->db = new PDO("mysql:host={$host};dbname={$dbname}", $username, $password);
    }
    public function add_data($email)
    {
        $data = $this->db->prepare('INSERT INTO sertifikat (email) VALUES (?)');
        
        $data->bindParam(1, $email);
        $data->execute();
        return $data->rowCount();
    }
    public function show()
    {
        $query = $this->db->prepare("SELECT * FROM peserta");
        $query->execute();
        $data = $query->fetchAll();
        return $data;
    }

    public function get_by_id($id){
        $query = $this->db->prepare("SELECT * FROM sertifikat where id=?");
        $query->bindParam(1, $id);
        $query->execute();
        return $query->fetch();
    }

    public function get_by_uniquecode($id){
        $query = $this->db->prepare("SELECT * FROM peserta where `uniquegenerate` = ?");
        $query->bindParam(1, $id);
        $query->execute();
        return $query->fetch();
    }

    public function cek_email($email)
    {
        $query = $this->db->prepare("SELECT * FROM peserta where email=?");
        $query->bindParam(1, $email);
        $query->execute();
        return $query->fetch();
    }

    public function update($id,$email){
        $query = $this->db->prepare('UPDATE sertifikat set email=? where id=?');
        
        $query->bindParam(1, $email);
        $query->bindParam(2, $id);

        $query->execute();
        return $query->rowCount();
    }

    public function addUrl($id, $url, $unique)
    {
        $query = $this->db->prepare("UPDATE `peserta` SET `qr-code` = ?, `uniquegenerate` = ? WHERE `peserta`.`id` = ?;");
        
        $query->bindParam(1, $url);
        $query->bindParam(2, $unique);
        $query->bindParam(3, $id);

        $query->execute();
        return $query->rowCount();
    }

    public function delete($id)
    {
        $query = $this->db->prepare("DELETE FROM peserta where id=?");

        $query->bindParam(1, $id);

        $query->execute();
        return $query->rowCount();
    }

}
?>